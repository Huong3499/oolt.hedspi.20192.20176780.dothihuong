package lab03;
import lab03.DigitalVideoDisc;
import lab03.Order;
public class Aims1 {
	public static void main(String[] args) {
		//  Auto-generated method stub
		Order anOrder = new Order();
		anOrder.setQtyOrdered(0);
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("xxx");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		if(anOrder.addDigitalVideoDisc(dvd1)) {
			System.out.println("Order Success");
			}else {
				System.out.println("Order Fail");
			}
			
		//dvd2
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("xxy");
		dvd2.setCategory("Animation");
		dvd2.setCost(24.95f);
		dvd2.setDirector("Roger Allers");
		dvd2.setLength(124);
		if(anOrder.addDigitalVideoDisc(dvd2)) {
			System.out.println("Order Success");
			}else {
				System.out.println("Order Fail");
			}
		//dvd3
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("xxz");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("Roger Allers");
		dvd3.setLength(90);
		if(anOrder.addDigitalVideoDisc(dvd3)) {
			System.out.println("Order Success");
			}else {
				System.out.println("Order Fail");
			}	
		if(anOrder.removeDigitalVideoDisc(dvd1)) {
			System.out.println("Delete Complete");
			}else {
				System.out.println("Delete Fail");
			}
		System.out.print("Total Cost is: ");
		System.out.println(anOrder.totalCost());
		}
	}


