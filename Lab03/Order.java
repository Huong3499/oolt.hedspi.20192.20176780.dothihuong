package lab03;
import lab03.DigitalVideoDisc;
public class Order {
	private int qtyOrdered;
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public boolean addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered > MAX_NUMBERS_ORDERED) return false;
		else {
			itemsOrdered[qtyOrdered] = disc;
			this.qtyOrdered ++;
			return true;
		}
	}
	public boolean removeDigitalVideoDisc(DigitalVideoDisc disc) { 
		int c, i;
		boolean result = false;
		for( c = i = 0; i < this.qtyOrdered ; i++) {
			if(this.itemsOrdered[i].getTitle() != disc.getTitle()) {
				this.itemsOrdered[c] = this.itemsOrdered[i];
				c++;
			}
		}
		if(this.qtyOrdered != c) {
			this.setQtyOrdered(c);
			result = true;
		}
		return result;
		
	 }

	public float totalCost() {
		float totalCost = 0;
		int i;
		for(i = 0; i < this.qtyOrdered; i++) {
			totalCost += itemsOrdered[i].getCost();
		}
		return totalCost;
	}
}



