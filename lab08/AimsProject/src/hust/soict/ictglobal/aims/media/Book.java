package hust.soict.ictglobal.aims.media;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
public class Book extends Media implements Comparable{
	public Book(String title, String category, float cost) {
		super(title, category,cost);
	}
	public Book() {
	}
	private String content;
	private List<String> authors = new ArrayList<String>();
	List<String> contentTokens;
	//private Map<String, Integer> wordFrequency = new HashMap<String, Integer>();
	private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();
	public static final char SPACE = ' ';
	public static final char TAB = '\t';
	public static final char BREAK_LINE = '\n';
	public static void processContent(String content) {
		// khởi tạo wordMap
		Map<String, Integer> wordMap = new TreeMap<String, Integer>();
		if (content == null) {
		}
		int size = content.length();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			if (content.charAt(i) != SPACE && content.charAt(i) != TAB 
					&& content.charAt(i) != BREAK_LINE) {
				// build một từ
				sb.append(content.charAt(i));
			} else {
				// thêm từ vào wordMap
				addWord(wordMap, sb);
				sb = new StringBuilder();
			}
		}
		// thêm từ cuối cùng tìm được vào wordMap
		addWord(wordMap, sb);
	}
	public static void addWord(Map<String, Integer> wordMap, StringBuilder sb) {
		String word = sb.toString();
        if (word.length() == 0) {
        }
        if (wordMap.containsKey(word)) {
            int count = wordMap.get(word) + 1;
            wordMap.put(word, count);
        } else {
            wordMap.put(word, 1);
       }
}
	public void processContent() {
	    contentTokens = Arrays.asList(content.split(" "));
	    Collections.sort(contentTokens);

	    System.out.println(contentTokens);

	    for (String contentToken : contentTokens) {
	      if (wordFrequency.containsKey(contentToken)) {
	        wordFrequency.put(contentToken, wordFrequency.get(contentToken) + 1);
	      } else {
	        wordFrequency.put(contentToken, 1);
	      }
	    }

	    for (Map.Entry entry : wordFrequency.entrySet()) {
	      System.out.println(entry.getKey() + " " + entry.getValue());
	    }
	  }

	public void addAuthor(String auth) {
		if(authors.contains(auth)==false)
		{
			this.authors.add(auth);
			System.out.println("them thanh cong khach hang :"+ auth);
		}else {
			System.out.println("them khong thanh cong "+auth +"da co trong danh sach ");
		}
	}
	public void removeAuthor(String auth) {
		if(authors.contains(auth)==false) {
			System.out.println(auth +"khong co trong danh sach ");
		}else {
			this.authors.remove(auth);
			System.out.println(auth +"da duoc xoa khoi danh sach");
		}
	}
	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		 this.content = content;
		 this.processContent();
	}
	@Override
	public int compareTo(Object arg0) {
		Book dvd = (Book) arg0;
		return this.getTitle().compareTo(dvd.getTitle());
	}
}
