package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;

public class Track implements Playable,Comparable{
	private String title ;
	private int length;
	public Track() {
		
	}
	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	@Override
	public void play() {
		System.out.println("Playing CDs: "+this.getTitle());
    	System.out.println("CD length: "+this.getLength());
		
	}
	@Override
	public int compareTo(Object arg0) {
		Track dvd = (Track) arg0;
		return this.getTitle().compareTo(dvd.getTitle());
	}
	
}
