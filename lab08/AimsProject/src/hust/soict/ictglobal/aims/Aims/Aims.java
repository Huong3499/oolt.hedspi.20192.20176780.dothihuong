package hust.soict.ictglobal.aims.Aims;
import java.util.*;
import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.*;
import hust.soict.ictglobal.aims.media.*;
import hust.soict.ictglobal.aims.order.Order.Order;
public class Aims {
	private static ArrayList<Order> listOrder = new ArrayList<Order>();
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4: ");
		}
	public static DigitalVideoDisc InputDVD(){
		DigitalVideoDisc dvd = new DigitalVideoDisc();
		Scanner d = new Scanner(System.in);
		System.out.print("Title: ");
		dvd.setTitle(d.nextLine());
		System.out.print("Category: ");
		dvd.setCategory(d.nextLine());
		System.out.print("Director: ");
		dvd.setDirector(d.nextLine());
		System.out.print("Length: ");
		dvd.setLength(Integer.parseInt(d.nextLine()));
		System.out.print("Cost: ");
		dvd.setCost(Float.parseFloat(d.nextLine()));
		return dvd;
	}
	public static CompactDisc InputCompactDisc(){
		CompactDisc cd = new CompactDisc();
		Scanner sc = new Scanner(System.in);
		System.out.print("Title: ");
		cd.setTitle(sc.nextLine());
		System.out.print("Category: ");
		cd.setCategory(sc.nextLine());
		System.out.print("Director: ");
		cd.setDirector(sc.nextLine());
		System.out.print("Artist: ");
		cd.setArtist(sc.nextLine());
		System.out.print("Cost: ");
		cd.setCost(Float.parseFloat(sc.nextLine()));
		int t=1;
		int i=1;
		while(t==1){
			Track track = new Track();
			System.out.print("add track with ");
			System.out.print("Title is"+ i +": ");
			track.setTitle(sc.nextLine());
			System.out.print("Length is "+ i +": ");
			track.setLength(Integer.parseInt(sc.nextLine()));
			cd.addTrack(track);
			i++;
			System.out.print("neu muon them track hay nhap 1. ban nhap :");
			t= Integer.parseInt(sc.nextLine());		
		}
		return cd;
	}
	public static Book InputBook(){
		Book book= new Book();
		Scanner b = new Scanner(System.in);
		System.out.print("Title: ");
		book.setTitle(b.nextLine());
		System.out.print("Category: ");
		book.setCategory(b.nextLine());
		System.out.print("Cost: ");
		book.setCost(Float.parseFloat(b.nextLine()));
		int t=1;
		int i=1;
		while(t==1){
			System.out.print("Author "+i+": ");
			String author= b.nextLine();
			book.addAuthor(author);
			i++;	
			System.out.print("Nhap vao 1 neu ban muon add. ban nhap : ");
			t= Integer.parseInt(b.nextLine());
			}
		return book;
	}
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		MemoryDaemon md = new MemoryDaemon();
	    Thread thread = new Thread(md);
	    thread.setDaemon(true);
		Scanner keyBoard = new Scanner(System.in);
		int c ;
		Order order=null;
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers",19.95f, 87);
	    DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas",24.95f, 124);
	    DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", "John Musker",18.99f, 90);
	    List<DigitalVideoDisc> discs = new ArrayList<DigitalVideoDisc>();  
	    discs.add(dvd2);
	    discs.add(dvd1);
	    discs.add(dvd3);
	    dvd1.play();
	    dvd2.play();
	    dvd3.play();
	   // java.util.Iterator  iterator = discs.iterator();
	    Iterator iterator = discs.iterator();
	    System.out.println("------------------------------------------");
	    System.out.println("The DVDs currently in the order are: ");
	    while (iterator.hasNext()) {
	      System.out.println(((DigitalVideoDisc) iterator.next()).getTitle());
	    }
	   // java.util. Collections.sort((java.util.List)discs);
	    Collections.sort((List)discs);
	    iterator = discs.iterator();
	    System.out.println("------------------------------------------");
	    System.out.println("The DVDs in sorted order are: ");
	    while (iterator.hasNext()) {
	      System.out.println(((DigitalVideoDisc) iterator.next()).getTitle());
	    }
	    System.out.println("------------------------------------------");
		do {
			 showMenu();
			 c = Integer.parseInt(keyBoard.nextLine());
			 switch(c) {
			 
			 	case 1:{
			 	    order= new Order();
			 	    listOrder.add(order);
					System.out.println("Created new item successfully");
			 	}break;
			 
			 	case 2:{
					int a = 0;
			 		System.out.println("Danh sach hien tai ");
			 		order.printListOrdered();
			 		do {
			 		System.out.println("1.Book - 2.CompactDisc - 3.DigitalVideoDisc . ban chon");
			 		a = Integer.parseInt(keyBoard.nextLine());
			 		switch (a) {
			 		case 1:{
						Book b=InputBook();
						listOrder.get(listOrder.size()-1).addMedia(b);
			 		}break;
			 		case 2:{
						CompactDisc cd=InputCompactDisc();
						listOrder.get(listOrder.size()-1).addMedia(cd);
			    		System.out.print("neu muon chay hay nhap 1. nhap vao:");
			    		int chon1 = Integer.parseInt(keyBoard.nextLine());
			    		if(chon1==1){
			    				cd.play();
			    			}
			 		}break;
					case 3:{
						DigitalVideoDisc dvd= InputDVD();
						listOrder.get(listOrder.size()-1).addMedia(dvd);
						System.out.print("neu muon chay hay nhap 1. nhap vao:");
			    		int chon2 = Integer.parseInt(keyBoard.nextLine());
			    		if(chon2==1){
			    				dvd.play();
			    			}
					}break;
					default:
						break;
						}
					}while(a == 1 || a==2 || a==3);
			 	}break;
			 	case 3:{
			 		System.out.println();
					System.out.println("Delete item by id");
					System.out.print("Enter the index you want to delete: ");
					int id=Integer.parseInt(keyBoard.nextLine());
					order.removeMediaByID(id);
			 	}break;
			 	case 4:{
			 		System.out.println("items list of order");
			 		order.printListOrdered();
			 	}break;
			 	default: {
			 		System.out.println("");
			 	}break;
			 }	 
		}while(c == 1 || c==2 || c==3 || c==4);
		
	}
}