package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import java.util.ArrayList;
import java.util.List;

import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.media.Playable;
import hust.soict.ictglobal.aims.media.Track;

public class CompactDisc extends Disc implements Playable,Comparable{
	private String  artist;
	private int  length;
	private List<Track> tracks = new ArrayList<Track>();
	public void addTrack(Track track) {
		if(tracks.contains(track)==true) {
			System.out.println(track.getTitle() +"da co trong danh sach");
		}else
			tracks.add(track);
			System.out.println(" them thanh cong "+ track.getTitle());
		
	}
	public void removeTrack(Track track) {
		if(tracks.contains(track)==true) {
			tracks.remove(track);
			System.out.println(" xoa thanh cong" + track.getTitle());
		}else
			System.out.println(track.getTitle() +"khong ton tai trong danh sach ");
	}
	
	public int getLength() {
		int sum = 0;
		for(Track i:tracks)
		{
			sum = sum + i.getLength();
		}
		return sum ;
		
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public CompactDisc() {
		
	}
	public CompactDisc(String artist, int string) {
		super();
		this.artist = artist;
		this.length = string;
	}
	@Override
	public void play() {
		System.out.println("Artist: "+this.getArtist());
    	System.out.println("CompactDisc length: "+this.getLength());
    	for(Track tr: tracks){
    		tr.play();
    	}
    }
	@Override
	public int compareTo(Object arg0) {
		CompactDisc dvd = (CompactDisc) arg0;
		if (this.tracks.size() > dvd.tracks.size())
			return 1;
		else if (this.tracks.size() < dvd.tracks.size()) 
			return -1;
		else {
			if (this.getLength() > dvd.getLength())
				return 1;
			else if (this.getLength() < dvd.getLength())
				return -1;
			return 0;
		}
//		    return this.getTitle().compareTo(dvd.getTitle());
	}
}
