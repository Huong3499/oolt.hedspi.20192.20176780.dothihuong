package hust.soict.ictglobal.garbage;
import java.nio.file.*;
import java.util.*; 
public class GarbageCreator {
	  
	 public static String ReadFile()throws Exception 
	  { 
	    String s = ""; 
	    Scanner scanner = new Scanner(Paths.get("/home/vst/Downloads/b4ctd/test/test.txt"),"UTF-8");
	    while(scanner.hasNextLine()){
	    	String tmp = scanner.nextLine();
	    	s = s + '+' + tmp;
	    }
	    scanner.close();
	    return s; 
	  } 

	public static void main(String[] args) throws Exception 
	  {
		  long start = System.currentTimeMillis();
		  String data = ReadFile(); 
		  System.out.println(System.currentTimeMillis()-start);
	  } 
} 
