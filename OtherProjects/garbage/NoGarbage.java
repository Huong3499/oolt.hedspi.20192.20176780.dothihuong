package hust.soict.ictglobal.garbage;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class NoGarbage {
	public static void main(String[] args) {
		File file = new File("//home/vst/Downloads/b4ctd/test/test.txt");
		String s = "";
		long start = System.currentTimeMillis();
		StringBuilder sb = new StringBuilder();
		start = System.currentTimeMillis();
		try (FileReader fr = new FileReader(file)) {
			int tmp;
			while ((tmp = fr.read()) != -1) {
				sb.append(tmp);
			}
			s = sb.toString();
			System.out.println(System.currentTimeMillis() - start);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

