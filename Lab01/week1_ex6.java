import javax.swing.JOptionPane;
public class week1_ex6 {
	public static void main(String[] args) {
		//phuong trinh bac nhat 1 an
		String S1, S2;
		System.out.println("phuong trinh bac nhat cos dang ax+b=0 ");
		S1 = JOptionPane.showInputDialog(null, "Hay nhap vao so a: ","nhap so thu nhat",JOptionPane.INFORMATION_MESSAGE);
		S2 =JOptionPane.showInputDialog(null, "Hay nhap so vao so b: ","nhap so thu hai",JOptionPane.INFORMATION_MESSAGE);
		Double str1 = Double.parseDouble(S1);
		Double str2 = Double.parseDouble(S2);
		System.out.println("nghiem cua phuong trinh bac nhat cua ban la x = "+(-str2/(str1*1.0f)));
		System.out.println("phuong trinh bac nhat co dang ax+by=c. Hay nhap vao cac gia tri a ,b, c ");
		//phuong trinh bac nhat 2 an
		String T1, T2, T3;
		T1 = JOptionPane.showInputDialog(null, "Hay nhap vao so a: ","nhap so thu nhat",JOptionPane.INFORMATION_MESSAGE);
		T2 =JOptionPane.showInputDialog(null, "Hay nhap so vao so b: ","nhap so thu hai",JOptionPane.INFORMATION_MESSAGE);
		T3 =JOptionPane.showInputDialog(null, "Hay nhap so vao so c: ","nhap so thu ba",JOptionPane.INFORMATION_MESSAGE);
		Double s1 = Double.parseDouble(T1);
		Double s2 = Double.parseDouble(T2);
		Double s3 = Double.parseDouble(T3);
		System.out.println("phuong trinh da cho co vo so nghiem x = " +s3 +"-" +s2 +"*y/"+ s1 );
		//phuong trinh bac hai 1 an
		String b1, b2, b3;
		System.out.println("phuong trinh bac nhat hai an co dang ax*x+bx+x=0");
		b1 = JOptionPane.showInputDialog(null, "Hay nhap vao so a: ","nhap so thu nhat",JOptionPane.INFORMATION_MESSAGE);
		b2 =JOptionPane.showInputDialog(null, "Hay nhap so vao so b: ","nhap so thu hai",JOptionPane.INFORMATION_MESSAGE);
		b3 =JOptionPane.showInputDialog(null, "Hay nhap so vao so c: ","nhap so thu hai",JOptionPane.INFORMATION_MESSAGE);
		Double a1 = Double.parseDouble(b1);
		Double a2 = Double.parseDouble(b2);
		Double a3 = Double.parseDouble(b3);
		Double deta= a2*a2-4*a1*a3;
		if(deta==0)
		{
			System.out.print("phuong trinh co nghiem kep = "+(a1 + Math.abs(deta))/(2*a1));
		}
		if(deta>0)
		{
			
			System.out.print("phuong trinh co hai nghiem x1 = "+(a1 + Math.abs(deta))/(2*a1));
			System.out.println(" va x2 = "+(a1 - Math.abs(deta))/(2*a1));
		}
		if(deta<0)
		{
			System.out.println("phuong trinh vo nghiem!");
		}
	}
}
