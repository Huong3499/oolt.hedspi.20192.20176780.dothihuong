package hust.soict.ictglobal.aims.media;

public class Media {
	private String title;
	private String category;
	private float cost;
	protected static int idTemp=1;
	protected int id;
	public Media(String title, String category, float cost,int id) {
		this.title=title;
		this.category=category;
		this.cost=cost;
		this.id=id;
	}
	public Media(String title, String category,float cost) {
		this.title=title;
		this.category=category;
		this.cost=cost;
	}
	public Media(String title, String category, String director, int length, float cost) {
		this.title=title;
		this.category=category;
		this.cost=cost;
		this.id=id;
	}
	public int getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.id = idTemp++;
		this.cost = cost;
	}
	

}
