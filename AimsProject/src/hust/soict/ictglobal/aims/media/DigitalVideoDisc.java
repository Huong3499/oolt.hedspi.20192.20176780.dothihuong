package hust.soict.ictglobal.aims.media;
public class DigitalVideoDisc extends Media {
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(title,category, director,length, cost);
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;

	}
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean search(String title) {
		title = title.toLowerCase();
		this.title = this.title.toLowerCase();
		if(this.title.contains(title)) {
			return true;
		}
		else {
			return false;
		}	
	}
}
