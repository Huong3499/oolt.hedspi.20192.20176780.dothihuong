package hust.soict.ictglobal.aims.Aims;
import java.util.ArrayList;
import java.util.Scanner;
import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.Order.Order;
public class Aims {
	private static ArrayList<Order> listOrder = new ArrayList<Order>();
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4: ");
		}
	public static void main(String[] args) {
		Scanner keyBoard = new Scanner(System.in);
		int c = 0;
		Order order=null;
		do {
			 showMenu();
			 c = Integer.parseInt(keyBoard.nextLine());
			 switch(c) {
			 
			 	case 1:{
			 	    order= new Order();
					System.out.println("Created new item successfully");
			 	}break;
			 
			 	case 2:{
			 		showOrderList();
			 		System.out.println();
					System.out.println("Add item to the order");
					System.out.println("-------------------------");
					System.out.print("Enter title: ");
					String title = keyBoard.nextLine();
					System.out.print("Enter category: ");
					String category = keyBoard.nextLine();
					System.out.print("Enter cost: ");
					float cost = Float.parseFloat(keyBoard.nextLine());
					System.out.print("Enter id: ");
					int id=Integer.parseInt(keyBoard.nextLine());
					Media item = new Media(title, category, cost,id);
        			order.addMedia(item);
			 	}break;
			 	case 3:{
			 		System.out.println();
					System.out.println("Delete item by id");
					System.out.print("Enter the index you want to delete: ");
					int id=Integer.parseInt(keyBoard.nextLine());
					order.removeMediaByID(id);
			 	}break;
			 	case 4:{
			 		System.out.println("items list of order");
			 		order.printListOrdered();
			 	}break;
			 	default: {
			 		System.out.println("");
			 	}break;
			 }	 
		}while(c == 1 || c==2 || c==3 || c==4);
		
	}
	private static void showOrderList() {
		for (Order order : listOrder) {
			System.out.println("Order ID: "+order.getIdOrder());
		}
	}
}