package hust.soict.ictglobal.aims.order.Order;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import hust.soict.ictglobal.aims.media.Media;
public class Order {
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders=0;
	private Date dateOrdered;
	private int idOrder;
	private int qtyOrdered;
	public static final int MAX_NUMBERS_ORDERED = 10;
	private static int idTemp=1;
	private List<Media> itemOrdered = new ArrayList<Media>();
	public Order(){
		super();
		this.dateOrdered = new Date();
		if(nbOrders<MAX_LIMITED_ORDERS){
			nbOrders++;
				System.out.println("The order "+nbOrders+" has been add");
			}else{
		System.out.println("The order hasnt been add");
				System.exit(-1);
			}
		}
	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public static int getIdTemp() {
		return idTemp;
	}

	public static void setIdTemp(int idTemp) {
		Order.idTemp = idTemp;
	}

	public static int getNbOrders() {
		return nbOrders;
	}

	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}
	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	private void setDateOrdered() {
		dateOrdered = new Date();
		
	}
	public void addMedia(Media media) {
		if (this.qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemOrdered.add(media);
			System.out.println("Them " + media.getTitle()+ " thanh cong!");
		}
		else {
			System.out.println("Them khong thanh cong");
		}
	}
	public void removeMedia(Media media) {
		if(this.qtyOrdered == 0) {
			System.out.println("Order rong:");
		}
		else {
			
			if ( itemOrdered.contains(media)==true) {
				itemOrdered.remove(media);
				System.out.println("Remove thanh cong"+media.getTitle());
			}
			else {
				System.out.println("Remove khong thanh cong, khong ton tai :"+media.getTitle());
			}
		}
	}
	public void removeMediaByID(int id) {
		if(itemOrdered.size() == 0) {
			System.out.println("Order empty!");
			return;
		}
		for (Media media : itemOrdered) {
			if(media.getId() == id) {
				itemOrdered.remove(media);
				System.out.println("Remove unsuccessful!");
				return;
			}
		}
		System.out.println("Media id "+id+" does not exist!");		
	}
	public void printListOrdered() {
		System.out.println("-------------Order------------");
		System.out.println("Date: " + dateOrdered);
		for (int i = 0; i < this.itemOrdered.size(); i++) {
			System.out.println( "DVD  ID: " 
					+ this.itemOrdered.get(i).getId() + " - " 
					+ this.itemOrdered.get(i).getTitle() + " - " 
					+ this.itemOrdered.get(i).getCategory()+ " - " 
					+ this.itemOrdered.get(i).getCost() + "$");
		}
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("--------------------------------");
		  }
	public float totalCost() {
		float total = 0;
		for (int i=0; i<itemOrdered.size();i++) {
			total+=itemOrdered.get(i).getCost();
		}
		return total;
	}
	public void addMedia1(Media media) {
		if (this.qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemOrdered.add(media);
			System.out.println("Them " + media.getTitle()+ " thanh cong!");
		}
		else {
			System.out.println("Them khong thanh cong");
		}
	}
	public static void main(String[] args) {
		Order x= new Order();
		x.printListOrdered();
	}
}


