package lab03;

import lab03.MyDate;

public class DateUtils {
	  public static int compareDate(MyDate date1, MyDate date2) {
	    int y = date1.getYear() - date2.getYear();
	    if (y > 0) {
	      System.out.println("date1 > date2");
	      return 1;
	    } else if (y == 0) {
	      int m = date1.getMon() - date2.getMon();
	      if (m > 0) {
	    	  System.out.println("date1 > date2");
	    	  return 1;
	      } else if (m == 0) {
	        int d = date1.getDay() - date2.getDay();
	        if (d > 0) {
	        	System.out.println("date1 > date2");
	        	return 1;
	        } else if (d == 0) {
	        	System.out.println("date1 = date2");
	        	return 0;
	        }
	      }
	    }
		return -1;
	  }
	  public static void Sorting(MyDate [] date) {
		  for(int i=0;i <date.length; i++) {
			  for(int j=0; j<date.length;i++) {
				 int k= compareDate(date[i], date[j]);
				 if(k!=1) {
					MyDate temp = date[i];
					date[i] =date[j];
					date[j] = temp;
			  }
		  }
		  }
	  }
}
