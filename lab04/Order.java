package lab03;
import java.util.Date;
import lab03.DigitalVideoDisc;
public class Order {
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders=0;
	private Date dateOrdered;
	private int qtyOrdered;
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public boolean addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered > MAX_NUMBERS_ORDERED) return false;
		else {
			itemsOrdered[qtyOrdered] = disc;
			this.qtyOrdered ++;
			return true;
		}
	}
	public boolean addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
	    if (qtyOrdered + dvdList.length > MAX_NUMBERS_ORDERED) 
	    {
	    	System.out.println("The order is almost full");
	    	return false;
	    }else {
	    	for (int i = 0; i < dvdList.length; i++) {
	        itemsOrdered[qtyOrdered] = dvdList[i];
	        qtyOrdered++;
	        System.out.println("The disc has been added");
	      }
	    }
		return true;
	  }
	public boolean addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
	    if (qtyOrdered + 2 >  MAX_NUMBERS_ORDERED) {
	    	 System.out.println("The order is almost full!");
	    	 return false;
	    }else {
	      itemsOrdered[qtyOrdered] = dvd1;
	      qtyOrdered++;
	      itemsOrdered[qtyOrdered] = dvd2;
	      qtyOrdered++;
	      System.out.println("The disc has been added!");
	    } 
	    return true;
	  }

	public boolean removeDigitalVideoDisc(DigitalVideoDisc disc) { 
		int c, i;
		boolean result = false;
		for( c = i = 0; i < this.qtyOrdered ; i++) {
			if(this.itemsOrdered[i].getTitle() != disc.getTitle()) {
				this.itemsOrdered[c] = this.itemsOrdered[i];
				c++;
			}
		}
		if(this.qtyOrdered != c) {
			this.setQtyOrdered(c);
			result = true;
		}
		return result;
		
	 }

	public float totalCost() {
		float totalCost = 0;
		int i;
		for(i = 0; i < this.qtyOrdered; i++) {
			totalCost += itemsOrdered[i].getCost();
		}
		return totalCost;
	}

	public static int getNbOrders() {
		return nbOrders;
	}

	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}
	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public Order() {
	    if (nbOrders == MAX_LIMITED_ORDERS) {
	      System.out.println("Max limited orders in day");
	      return;
	    }
	    this.setDateOrdered();
	    nbOrders++;
	  }
	private void setDateOrdered() {
		dateOrdered = new Date();
		
	}

	public void printOrder() {
	    System.out.println("*****************Order******************");
	    System.out.println("Date: " + dateOrdered);
	    System.out.println("Ordered items:");
	    for (int i = 0; i < qtyOrdered; i++) {
	      System.out.println(i + 1 + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": " + itemsOrdered[i].getCost() + "$");
	    }
	    System.out.println("Total cost: " + this.totalCost());
	    System.out.println("****************************************");
	  }
}




