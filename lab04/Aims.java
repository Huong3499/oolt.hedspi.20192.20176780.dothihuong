package lab03;
import lab03.DigitalVideoDisc;
import lab03.Order;
public class Aims {
	public static void main(String[] args) {
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);
		//dvd2
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("haha");
		dvd2.setCategory("Animation");
		dvd2.setCost(24.95f);
		dvd2.setDirector("Roger Allers");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		//dvd3
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("hehe");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("Roger Allers");
		dvd3.setLength(90);
		anOrder.addDigitalVideoDisc(dvd3);
		//System.out.print("Total Cost is: ");
		//System.out.println(anOrder.totalCost());
		anOrder.printOrder();
		}
}
	