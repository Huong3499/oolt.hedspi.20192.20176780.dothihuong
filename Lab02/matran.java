import java.util.Scanner;
public class matran {
	public static void main(String[] args) {
		int m1, n1;
	    Scanner scanner = new Scanner(System.in);
	    System.out.println("Nhập vào số dòng của hai ma trận : ");
	    m1 = scanner.nextInt();
	    System.out.println("Nhập vào số cột của hai ma trận: ");
	    n1 = scanner.nextInt();
	    int[][] A = new int[m1][n1];
	    int[][] B = new int[m1][n1];
	    System.out.println("Nhập vào các phần tử của ma trận A: ");
	    for (int i = 0; i < m1; i++) {
	    	for (int j = 0; j < n1; j++) {
	    		System.out.print("A[" + i + "]["+ j + "] = ");
	    		A[i][j] = scanner.nextInt();
	    	}
	    }
	    System.out.println("Nhập vào các phần tử của ma trận B: ");
	    for (int i = 0; i < m1; i++) {
	    	for (int j = 0; j < n1; j++) {
	    		System.out.print("B[" + i + "]["+ j + "] = ");
	    		B[i][j] = scanner.nextInt();
	    	}
	    }
	    System.out.println("Ma trận A: ");
	    for (int i = 0; i < m1; i++) {
	    	for (int j = 0; j < n1; j++) {
	    		System.out.print(A[i][j] + "\t");
	    	}
	    	System.out.println("\n");
	    }
	    
	    	System.out.println("Ma trận B: ");
	    	for (int i = 0; i < m1; i++) {
	    		for (int j = 0; j < n1; j++) {
	    			System.out.print(B[i][j] + "\t");
	                	}
	                System.out.println("\n");
	      }          
	   }
	}
	    
	