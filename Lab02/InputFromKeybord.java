import java.util.Scanner;
public class InputFromKeybord {
	public static void main (String args[]) {
		Scanner Keyboard = new Scanner(System.in);
		System.out.println("what's your name?");
		String name = Keyboard.nextLine();
		System.out.println("how old are you?");
		int age = Keyboard.nextInt();
		System.out.println("how tall are you (m)?");
		double height = Keyboard.nextDouble();
		System.out.println("Mrs/Ms: " + name + "," + age +" years old. " + "," + "Your height is " + height +"m.");
		
	}

}
