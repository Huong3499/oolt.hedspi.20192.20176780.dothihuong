package hust.soict.ictglobal.gui.awt;
import java.awt.*;
import java.awt.event.*;
@SuppressWarnings({ "unused", "serial" })
public class AWTCounter extends Frame implements ActionListener{
	
	private Label lblCounter;
	private TextField tfCount;
	private Button btnCount; 
	private int count = 0; 
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AWTCounter app = new AWTCounter();
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		++count;
		tfCount.setText(count + "");
		
	}
	public AWTCounter () {
		setLayout(new FlowLayout());
		lblCounter = new Label("Counter");
		add(lblCounter);
		tfCount = new TextField(count + "", 10); 
		tfCount.setEditable(false);
		add(tfCount); 
		btnCount = new Button("Count");
		add(btnCount);
		btnCount.addActionListener(this);
		setTitle("AWT Counter"); 
		setSize(250, 100); 
		setVisible(true);	
	}

}
