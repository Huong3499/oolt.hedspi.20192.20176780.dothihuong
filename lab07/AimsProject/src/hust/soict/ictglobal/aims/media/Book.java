package hust.soict.ictglobal.aims.media;
import java.util.ArrayList;
import java.util.List;
public class Book extends Media {
	public Book(String title, String category, float cost) {
		super(title, category,cost);
	}
	public Book() {
	}
	private String title;
	private String category;
	private float cost;
	private List<String> authors = new ArrayList<String>();
	public void addAuthor(String auth) {
		if(authors.contains(auth)==false)
		{
			this.authors.add(auth);
			System.out.println("them thanh cong khach hang :"+ auth);
		}else {
			System.out.println("them khong thanh cong "+auth +"da co trong danh sach ");
		}
	}
	public void removeAuthor(String auth) {
		if(authors.contains(auth)==false) {
			System.out.println(auth +"khong co trong danh sach ");
		}else {
			this.authors.remove(auth);
			System.out.println(auth +"da duoc xoa khoi danh sach");
		}
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
}
