package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import java.util.ArrayList;
import java.util.List;

import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.media.Playable;
import hust.soict.ictglobal.aims.media.Track;

public class CompactDisc extends Disc implements Playable{
	private String  artist;
	private int  length;
	private List<Track> tracks = new ArrayList<Track>();
	public void addTrack(Track track) {
		if(tracks.contains(track)==true) {
			System.out.println(track.getTitle() +"da co trong danh sach");
		}else
			tracks.add(track);
			System.out.println(" them thanh cong "+ track.getTitle());
		
	}
	public void removeTrack(Track track) {
		if(tracks.contains(track)==true) {
			tracks.remove(track);
			System.out.println(" xoa thanh cong" + track.getTitle());
		}else
			System.out.println(track.getTitle() +"khong ton tai trong danh sach ");
	}
	
	public int getLength() {
		int sum = 0;
		for(Track i:tracks)
		{
			sum = sum + i.getLength();
		}
		return sum ;
		
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public CompactDisc() {
		
	}
	@Override
	public void play() {
		System.out.println("Artist: "+this.getArtist());
    	System.out.println("CompactDisc length: "+this.getLength());
    	for(Track tr: tracks){
    		tr.play();
    	}
    }

}
