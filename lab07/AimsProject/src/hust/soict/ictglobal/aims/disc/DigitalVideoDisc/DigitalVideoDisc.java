package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Playable;
public class DigitalVideoDisc  extends Disc implements Playable{
	public DigitalVideoDisc(String title, String category, String director,float cost, int length) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	public DigitalVideoDisc(){
	}
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean search(String title) {
		title = title.toLowerCase();
		this.title = this.title.toLowerCase();
		if(this.title.contains(title)) {
			return true;
		}
		else {
			return false;
		}	
	}
	@Override
	public void play() {
		System.out.println("Playing DVD: "+this.getTitle());
    	System.out.println("DVD length: "+this.getLength());
		
	}
}
